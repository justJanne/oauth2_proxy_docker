FROM golang:1.12 as builder
RUN CGO_ENABLED=0 go get -d -v github.com/bitly/oauth2_proxy
RUN CGO_ENABLED=0 go install -v github.com/bitly/oauth2_proxy

FROM alpine
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/bin/oauth2_proxy /

EXPOSE 8080 4180

ENTRYPOINT [ "/oauth2_proxy" ]
CMD [ "-upstream=http://0.0.0.0:8080/", "-http-address=0.0.0.0:4180" ]
