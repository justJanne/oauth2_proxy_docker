#!/bin/sh
IMAGE=k8r.eu/justjanne/oauth2_proxy_docker
REPO=https://github.com/bitly/oauth2_proxy
TAGS=$(./git-describe-remote.sh $REPO)

docker build -t $IMAGE:$TAGS .
docker tag $IMAGE:$TAGS $IMAGE:latest
echo Successfully tagged $IMAGE:latest
docker push $IMAGE:$TAGS
docker push $IMAGE:latest
