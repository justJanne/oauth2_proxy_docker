#/bin/sh

FOLDER=/tmp/$(uuidgen)
REPO=$1

# Cleanup  
rm -rf $FOLDER
mkdir $FOLDER
cd $FOLDER

# Init repo
git init > /dev/null 2>&1
git remote add origin $REPO > /dev/null 2>&1
git fetch --dry-run > /dev/null 2>&1
git fetch --tags > /dev/null 2>&1

# Actually describe the tags
git describe --tags origin/master

# Cleanup
rm -rf $FOLDER
